require 'test_helper'

class MvQuantitiesControllerTest < ActionController::TestCase
  setup do
    @mv_quantity = mv_quantities(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:mv_quantities)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create mv_quantity" do
    assert_difference('MvQuantity.count') do
      post :create, mv_quantity: { good_id: @mv_quantity.good_id, qty: @mv_quantity.qty, store_id: @mv_quantity.store_id }
    end

    assert_redirected_to mv_quantity_path(assigns(:mv_quantity))
  end

  test "should show mv_quantity" do
    get :show, id: @mv_quantity
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @mv_quantity
    assert_response :success
  end

  test "should update mv_quantity" do
    patch :update, id: @mv_quantity, mv_quantity: { good_id: @mv_quantity.good_id, qty: @mv_quantity.qty, store_id: @mv_quantity.store_id }
    assert_redirected_to mv_quantity_path(assigns(:mv_quantity))
  end

  test "should destroy mv_quantity" do
    assert_difference('MvQuantity.count', -1) do
      delete :destroy, id: @mv_quantity
    end

    assert_redirected_to mv_quantities_path
  end
end
