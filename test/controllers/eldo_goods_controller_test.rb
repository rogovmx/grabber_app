require 'test_helper'

class EldoGoodsControllerTest < ActionController::TestCase
  setup do
    @eldo_good = eldo_goods(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:eldo_goods)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create eldo_good" do
    assert_difference('EldoGood.count') do
      post :create, eldo_good: { bonus: @eldo_good.bonus, brend: @eldo_good.brend, category_id: @eldo_good.category_id, foreign_id: @eldo_good.foreign_id, image: @eldo_good.image, is_yandex_model: @eldo_good.is_yandex_model, price: @eldo_good.price, short_name: @eldo_good.short_name, title: @eldo_good.title, url: @eldo_good.url, yandex_id: @eldo_good.yandex_id }
    end

    assert_redirected_to eldo_good_path(assigns(:eldo_good))
  end

  test "should show eldo_good" do
    get :show, id: @eldo_good
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @eldo_good
    assert_response :success
  end

  test "should update eldo_good" do
    patch :update, id: @eldo_good, eldo_good: { bonus: @eldo_good.bonus, brend: @eldo_good.brend, category_id: @eldo_good.category_id, foreign_id: @eldo_good.foreign_id, image: @eldo_good.image, is_yandex_model: @eldo_good.is_yandex_model, price: @eldo_good.price, short_name: @eldo_good.short_name, title: @eldo_good.title, url: @eldo_good.url, yandex_id: @eldo_good.yandex_id }
    assert_redirected_to eldo_good_path(assigns(:eldo_good))
  end

  test "should destroy eldo_good" do
    assert_difference('EldoGood.count', -1) do
      delete :destroy, id: @eldo_good
    end

    assert_redirected_to eldo_goods_path
  end
end
