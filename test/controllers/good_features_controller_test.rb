require 'test_helper'

class GoodFeaturesControllerTest < ActionController::TestCase
  setup do
    @good_feature = good_features(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:good_features)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create good_feature" do
    assert_difference('GoodFeature.count') do
      post :create, good_feature: { good_id: @good_feature.good_id, name: @good_feature.name, sign: @good_feature.sign, title: @good_feature.title }
    end

    assert_redirected_to good_feature_path(assigns(:good_feature))
  end

  test "should show good_feature" do
    get :show, id: @good_feature
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @good_feature
    assert_response :success
  end

  test "should update good_feature" do
    patch :update, id: @good_feature, good_feature: { good_id: @good_feature.good_id, name: @good_feature.name, sign: @good_feature.sign, title: @good_feature.title }
    assert_redirected_to good_feature_path(assigns(:good_feature))
  end

  test "should destroy good_feature" do
    assert_difference('GoodFeature.count', -1) do
      delete :destroy, id: @good_feature
    end

    assert_redirected_to good_features_path
  end
end
