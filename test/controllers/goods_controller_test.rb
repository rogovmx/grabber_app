require 'test_helper'

class GoodsControllerTest < ActionController::TestCase
  setup do
    @good = goods(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:goods)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create good" do
    assert_difference('Good.count') do
      post :create, good: { bonus: @good.bonus, brend: @good.brend, category_id: @good.category_id, foreign_id: @good.foreign_id, image: @good.image, is_yandex_model: @good.is_yandex_model, price: @good.price, short_name: @good.short_name, title: @good.title, url: @good.url, yandex_id: @good.yandex_id }
    end

    assert_redirected_to good_path(assigns(:good))
  end

  test "should show good" do
    get :show, id: @good
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @good
    assert_response :success
  end

  test "should update good" do
    patch :update, id: @good, good: { bonus: @good.bonus, brend: @good.brend, category_id: @good.category_id, foreign_id: @good.foreign_id, image: @good.image, is_yandex_model: @good.is_yandex_model, price: @good.price, short_name: @good.short_name, title: @good.title, url: @good.url, yandex_id: @good.yandex_id }
    assert_redirected_to good_path(assigns(:good))
  end

  test "should destroy good" do
    assert_difference('Good.count', -1) do
      delete :destroy, id: @good
    end

    assert_redirected_to goods_path
  end
end
