require 'test_helper'

class MvStoresControllerTest < ActionController::TestCase
  setup do
    @mv_store = mv_stores(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:mv_stores)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create mv_store" do
    assert_difference('MvStore.count') do
      post :create, mv_store: { code: @mv_store.code, title: @mv_store.title }
    end

    assert_redirected_to mv_store_path(assigns(:mv_store))
  end

  test "should show mv_store" do
    get :show, id: @mv_store
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @mv_store
    assert_response :success
  end

  test "should update mv_store" do
    patch :update, id: @mv_store, mv_store: { code: @mv_store.code, title: @mv_store.title }
    assert_redirected_to mv_store_path(assigns(:mv_store))
  end

  test "should destroy mv_store" do
    assert_difference('MvStore.count', -1) do
      delete :destroy, id: @mv_store
    end

    assert_redirected_to mv_stores_path
  end
end
