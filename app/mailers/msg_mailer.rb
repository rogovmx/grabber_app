class MsgMailer < ApplicationMailer
  
  def contact_email(name, email, message)
    @name = name
    @email = email
    @message = message
    mail(from: @email,
         to: 'rogovmx@gmail.com',
         subject: 'Msg\'s Email')
  end
  
end
