# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).ready ->
  $('#eldo_subcategory_id').chained '#eldo_category_id'

  $('#eldo_category_id').bind 'change', ->
    id = $(this).val()
    url = '/eldo_goods?subcategory_id=' + id
    window.location.replace url

  $('#eldo_subcategory_id').bind 'change', ->
    id = $(this).val()
    if !!id
      url = '/eldo_goods?subcategory_id=' + id
    else  
      id = $('#eldo_category_id').val()
      url = '/eldo_goods?subcategory_id=' + id
    window.location.replace url
 





  $('#eldo_without_yandex_id').bind 'change', ->
    url = '/eldo_goods?subcategory_id=' + $('#eldo_subcategory_id').val() + '&eldo_without_ym_id=' + $('#eldo_without_yandex_id').is ':checked'
    window.location.replace url

