# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

String::strip = -> if String::trim? then @trim() else @replace /^\s+|\s+$/g, ""

$(document).ready ->
  #$("#popup").dialog { autoOpen: false, modal: true }
  $(".wice-grid a.btn-primary").click ->
    $("#popup").hide()
    $("#waiting").show()
  $("#popup").draggable handle: "#popup_head"


  $("#popup_close").click ->
    $("#popup_content").html ""
    $("#popup").hide()
    return

  $("#popup_close_btn").on 'click', ->
    $("#popup_content").html ""
    $("#popup").hide()
    

  $('#entity_type_id').bind 'change', ->
    url = '/products?entity_type_id=' + $(this).val()
    window.location.replace url
    false 

  $('#entity_type_scan_id').bind 'change', ->
    url = '/products/scan_models?entity_type_id=' + $(this).val()
    window.location.replace url
    false 

  $('#without_yandex_id').bind 'change', ->
    url = '/products?entity_type_id=' + $('#entity_type_id').val() + '&without_ym_id=' + $('#without_yandex_id').is ':checked'
    window.location.replace url
    #false 

  $('#in_stock').bind 'change', ->
    url = '/products?entity_type_id=' + $('#entity_type_id').val() + '&in_stock=' + $('#in_stock').is ':checked'
    window.location.replace url
    #false 
    
  #$('#products').DataTable
  #  lengthMenu: [[ 30, 60, 100, -1], [ 30, 60, 100, "Все"]]