# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).ready ->
  $('#subcategory_id').chained '#category_id'

  $('#category_id').bind 'change', ->
    id = $(this).val()
    url = '/mv_goods?subcategory_id=' + id
    window.location.replace url
    false 

  $('#subcategory_id').bind 'change', ->
    id = $(this).val()
    if !!id
      url = '/mv_goods?subcategory_id=' + id
    else
      id = $('#category_id').val()
      url = '/mv_goods?subcategory_id=' + id
    window.location.replace url
    false 

  $('#mv_without_yandex_id').bind 'change', ->
    url = '/mv_goods?subcategory_id=' + $('#subcategory_id').val() + '&mv_without_ym_id=' + $('#mv_without_yandex_id').is ':checked'
    window.location.replace url