class EldoCategory < Eldorado
  self.table_name = :categories
  
  acts_as_nested_set
  
  default_scope { order(:title)}
  
  validates :title, presence: true, uniqueness: true
  
  #has_many :category_features
  has_many :eldo_goods, foreign_key: "category_id"

  scope :subcategories, -> { where.not(parent_id: nil)}
end
