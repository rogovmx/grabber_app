class Eldorado < ActiveRecord::Base
  self.abstract_class = true
  establish_connection configurations["eldorado"]
end
