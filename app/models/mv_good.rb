class MvGood < Mvideo
  self.table_name = :goods
  
  validates :title, presence: true
  validates :category_id, presence: true

  has_many :mv_good_features, foreign_key: "good_id"
  has_many :mv_quantities, foreign_key: "good_id"
  belongs_to :mv_category, foreign_key: "category_id"
  
  scope :found_in_yandex, -> {where.not(yandex_id: nil)}
  scope :not_found_in_yandex, -> {where(yandex_id: nil)}
  scope :no_features, -> {joins(:mv_good_features).where("good_features.good_id IS NULL")}
  
      
  def self.dedupe
    grouped = all.group_by{|good| [good.foreign_id] }
    grouped.values.each do |duplicates|
      first_one = duplicates.shift
      duplicates.each{|double| double.destroy}
    end
  end
  
end
