class EntityType < WorkBase
  
  scope :visible, -> { where(is_product: 1).order(:display_name)}
  scope :visible_with_products, -> { visible.select('entity_types.id, display_name').includes(:products).find_all { |et| et.products.size > 0 }}
  default_scope { visible }
  has_many :products
  
end
