class SubVendor < WorkBase
  
  
  has_many :products, :class_name => 'Product', :foreign_key => 'vendor_id'
end
