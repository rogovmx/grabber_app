class Product < WorkBase
#  scope :for_stock, -> { where("stock_quantity > 0")}
  scope :for_stock, -> { where("delivery_order = 1 or delivery_order = 2")}
  default_scope { for_stock }
  scope :no_yandex_id, -> {where("yandex_modelid IS NULL OR yandex_modelid = 0")}
  scope :in_stock, -> {where("stock_quantity > 0")}
  scope :scanned, -> {no_yandex_id.where(id: ScanModel.all.pluck(:product_id))}
  
  belongs_to :entity_type
  belongs_to :vendor, class_name: "SubVendor", foreign_key: "vendor_id"
  has_one :scan_model
  
  
end
