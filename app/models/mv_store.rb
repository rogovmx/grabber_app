class MvStore < Mvideo
  self.table_name = :stores
  
  validates :title, presence: true
  validates :code, presence: true, uniqueness: true
end
