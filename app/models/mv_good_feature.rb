class MvGoodFeature < Mvideo
  self.table_name = :good_features
  
  validates :name, :title, presence: true
  
  belongs_to :mv_good, foreign_key: "good_id"
end
