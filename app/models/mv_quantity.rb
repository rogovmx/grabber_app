class MvQuantity < Mvideo
  self.table_name = :quantities
  
  validates :good_id, presence: true
  validates :store_id, presence: true

  belongs_to :mv_store, foreign_key: "store_id"
  belongs_to :mv_good, foreign_key: "good_id"
end
