class MvGoodsController < ApplicationController
  before_action :set_good, only: [:show, :edit, :update, :destroy, :reload_form, :custom_search]

  def index
    @categories = MvCategory.all
#    @subcategories = MvCategory.subcategories
    session[:mv_without_ym_id] = true if params[:mv_without_ym_id] == 'true'
    session[:mv_without_ym_id] = false if params[:mv_without_ym_id] == 'false'
    session[:mv_grid] = params[:grid]
    
    @all_goods_count = MvGood.count
    session[:mv_subcategory_id] = params[:subcategory_id] unless params[:subcategory_id].blank?
    session[:mv_subcategory_id] = nil if params[:show_all] == 'true'
    
    unless session[:mv_subcategory_id].blank?
      @subcategory = @categories.find session[:mv_subcategory_id].to_i
      @category = 
        if @subcategory.root?
          @subcategory
        else
          @categories.find @subcategory.parent_id.to_i 
        end
        
      @goods = 
        if @subcategory.root?
          MvGood.where(category_id: @subcategory.children.map{|c| c.id})
        else
          @subcategory.mv_goods          
        end
    else  
      @goods = MvGood.all
    end  
    
    @goods = @goods.not_found_in_yandex if session[:mv_without_ym_id]
    
    if session[:mv_grid].blank?
      @grid = initialize_grid(@goods, 
        name: 'g1',
        per_page: 20,
        enable_export_to_csv: true,
        csv_file_name: "mvideo_#{Time.now}",
        order: "price",
        order_direction: 'desc'
        )
    else
      grid = 
        if session[:mv_grid].is_a? String
          JSON.parse(session[:mv_grid])
        else
          session[:mv_grid]
        end
      @grid = initialize_grid(@goods, 
        name: 'g1',
        per_page: 20,
        order: "#{grid[:order] || 'price'}",
        order_direction: "#{grid[:order_direction]  || 'desc'}",
        enable_export_to_csv: true,
        csv_file_name: "mvideo_#{Time.now}"
      )
    end
    

    export_grid_if_requested('g1' => 'mv_goods_table_wice') do
      # usual render or redirect code executed if the request is not a CSV export request
    end
  end

  def show
    @result = get_model_info_from_yandex @good.yandex_id unless @good.yandex_id.blank?
  end

  def new
    @good = Good.new
  end

  def edit
  end

  def create
    @good = Good.new(good_params)

    respond_to do |format|
      if @good.save
        format.html { redirect_to mv_goods_url, notice: 'Good was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end


  def update
    respond_to do |format|
      if @good.update(good_params)
        format.html { redirect_to mv_goods_url, notice: 'Good was successfully updated.' }
      else
        format.html { render :show }
      end
    end
  end


  def destroy
    @good.destroy
    respond_to do |format|
      format.html { redirect_to mv_goods_url, notice: 'Good was successfully destroyed.' }
    end
  end
  
  def custom_search
    @searched = get_models_from_yandex URI.encode params[:search]
  end

  
  def reload_form; end
  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_good
      @good = MvGood.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def good_params
      params.require(:mv_good).permit(:title, :brend, :short_name, :yandex_id)
    end
end
