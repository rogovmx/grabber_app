class MvGoodFeaturesController < ApplicationController
  before_action :set_good_feature, only: [:show, :edit, :update, :destroy]

  # GET /good_features
  # GET /good_features.json
  def index
    @good_features = GoodFeature.all
  end

  # GET /good_features/1
  # GET /good_features/1.json
  def show
  end

  # GET /good_features/new
  def new
    @good_feature = GoodFeature.new
  end

  # GET /good_features/1/edit
  def edit
  end

  # POST /good_features
  # POST /good_features.json
  def create
    @good_feature = GoodFeature.new(good_feature_params)

    respond_to do |format|
      if @good_feature.save
        format.html { redirect_to @good_feature, notice: 'Good feature was successfully created.' }
        format.json { render :show, status: :created, location: @good_feature }
      else
        format.html { render :new }
        format.json { render json: @good_feature.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /good_features/1
  # PATCH/PUT /good_features/1.json
  def update
    respond_to do |format|
      if @good_feature.update(good_feature_params)
        format.html { redirect_to @good_feature, notice: 'Good feature was successfully updated.' }
        format.json { render :show, status: :ok, location: @good_feature }
      else
        format.html { render :edit }
        format.json { render json: @good_feature.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /good_features/1
  # DELETE /good_features/1.json
  def destroy
    @good_feature.destroy
    respond_to do |format|
      format.html { redirect_to good_features_url, notice: 'Good feature was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_good_feature
      @good_feature = GoodFeature.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def good_feature_params
      params.require(:good_feature).permit(:good_id, :title, :name, :sign)
    end
end
