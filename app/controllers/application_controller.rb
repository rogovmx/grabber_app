class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  #require 'will_paginate/array'
  http_basic_authenticate_with name: "polus", password: "polus" #, except: :index
  
  AUTH = "&oauth_token=c883ee85fe574b8d8d5d2ca62fd9b7e6&oauth_client_id=ac448773268a4ee49e954b09c779f319&oauth_login=eders1978"
  require "net/https"
  require "uri"
  
  protected
  
    def find_product_yandex product
      result = []
      @results = []
      queries = []
      vendor = product.vendor ? product.vendor.name.to_s.strip : ''
      name = product.short_name.to_s.strip.blank? ? product.name.to_s.strip : product.short_name.to_s.strip
      vendor_code = product.vendor_code.to_s.strip
      model = product['model_name'].to_s.strip


      queries << [(vendor + ' ' + name).strip, "Производитель + кор. имя | имя"] unless (vendor + ' ' + name).strip.blank?
      queries << [(vendor + ' ' + vendor_code).strip, "Производитель + код производителя"] unless (vendor + ' ' + vendor_code).strip.blank? || vendor_code.strip.blank?
      queries << [model, "Модель"] unless model.blank?
      queries << [vendor_code, ""] unless vendor_code.blank?
      queries << [(name.split(' ').take(3).join(' ')).strip, "кор. имя | имя - 3 слова"] unless (name.split(' ').take(3).join(' ')).strip.blank?
      queries << [(name.split(' ').take(4).join(' ')).strip, "кор. имя | имя - 4 слова"] unless (name.split(' ').take(4).join(' ')).strip.blank?
      queries.uniq! { |q| q.first }
      queries.each do |query, descr|
        q = query.gsub(" ", "+")
        unless q.blank?
          resp = get_models_from_yandex( URI.encode q )
          if resp && resp['pager'] && resp['pager']['total'] && resp['pager']['total'] == 1
            result << [q, resp, descr] 
            break
          elsif resp && resp['pager'] && resp['pager']['total'] && resp['pager']['total'] > 1
            @results << [q, resp, descr] 
          end
        end
      end
      @results.uniq! { |r| r.second['id'] }
      result.uniq! { |r| r.second['id'] }
      result
    end
  
    def get_models_from_yandex query

      @url = "https://api.partner.market.yandex.ru/v2/models.json?regionId=2&query=#{query}" + AUTH
      
      uri = URI.parse(@url)
      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      request = Net::HTTP::Get.new(uri.request_uri)
      response = http.request(request)

      #response = Net::HTTP.get_response(URI.parse @url)
#      @resp = JSON.parse(response.body.to_s.force_encoding(Encoding::UTF_8))
      resp = response.body
#      resp = RestClient.get(@url, @headers)
    #rescue  
      File.open("log/models.log", 'a') do |file| 
        unless response.code == "200"
          file.write response.code;
          file.write response.body.inspect; file.write "\n" 
        end  
      end
      JSON.parse(resp)
#      .force_encoding(Encoding::UTF_8)
    rescue => error
      File.open("log/models.log", 'a') do |file| 
          file.write error.inspect 
          file.write "\n" 
#          file.write $!.message; file.write "\n" 
      end
      nil
    end
    
  def get_model_info_from_yandex model
    @url = "https://api.partner.market.yandex.ru/v2/models/#{model}.json?regionId=2" + AUTH
    uri = URI.parse(@url)
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    request = Net::HTTP::Get.new(uri.request_uri)
    response = http.request(request)
    resp = response.body
    JSON.parse(resp)
  end
  
end



