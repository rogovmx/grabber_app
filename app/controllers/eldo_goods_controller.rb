class EldoGoodsController < ApplicationController
  before_action :set_eldo_good, only: [:show, :edit, :update, :destroy, :reload_form, :custom_search]

  def index
    @categories = EldoCategory.all
#    @subcategories = EldoCategory.subcategories
    session[:eldo_without_ym_id] = true if params[:eldo_without_ym_id] == 'true'
    session[:eldo_without_ym_id] = false if params[:eldo_without_ym_id] == 'false'
    session[:eldo_grid] = params[:grid]
    
    session[:eldo_subcategory_id] = params[:subcategory_id] unless params[:subcategory_id].blank?
    session[:eldo_subcategory_id] = nil if params[:show_all] == 'true'
    
    @all_goods_count = EldoGood.count
    
    
    unless session[:eldo_subcategory_id].blank?
      @subcategory = @categories.find session[:eldo_subcategory_id].to_i
      @category = 
        if @subcategory.root?
          @subcategory
        else
          @categories.find @subcategory.parent_id.to_i 
        end

      @goods = 
        if @subcategory.root?
          EldoGood.where(category_id: @subcategory.children.map{|c| c.id})
        else
          @subcategory.eldo_goods          
        end       
    else  
      @goods = EldoGood.all
    end  
    
    @goods = @goods.not_found_in_yandex if session[:eldo_without_ym_id]
    
    if session[:eldo_grid].blank?
      @grid = initialize_grid(@goods, 
        name: 'g1',
        per_page: 20,
        enable_export_to_csv: true,
        csv_file_name: "eldorado_#{Time.now}",
        order: "price",
        order_direction: 'desc'
        )
    else
      grid = 
        if session[:eldo_grid].is_a? String
          JSON.parse(session[:eldo_grid])
        else
          session[:eldo_grid]
        end
      @grid = initialize_grid(@goods, 
        name: 'g1',
        per_page: 20,
        order: "#{grid[:order] || 'price'}",
        order_direction: "#{grid[:order_direction]  || 'desc'}",
        enable_export_to_csv: true,
        csv_file_name: "eldorado_#{Time.now}"
      )
    end
    

    export_grid_if_requested('g1' => 'eldo_goods_table_wice') do
      # usual render or redirect code executed if the request is not a CSV export request
    end

  end

  def show
    @result = get_model_info_from_yandex @good.yandex_id unless @good.yandex_id.blank?
  end

  def new
    @good = EldoGood.new
  end

  def edit
  end


  def create
    @good = EldoGood.new(eldo_good_params)

    respond_to do |format|
      if @good.save
        format.html { redirect_to eldo_goods_url, notice: 'Eldo good was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end


  def update
    respond_to do |format|
      if @good.update(eldo_good_params)
        format.html { redirect_to eldo_goods_url, notice: 'Eldo good was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end


  def destroy
    @good.destroy
    respond_to do |format|
      format.html { redirect_to eldo_goods_url, notice: 'Eldo good was successfully destroyed.' }
    end
  end
  
  def custom_search
    @searched = get_models_from_yandex URI.encode params[:search]
  end

  
  def reload_form; end

  private

    def set_eldo_good
      @good = EldoGood.find(params[:id])
    end


    def eldo_good_params
      params.require(:eldo_good).permit(:foreign_id, :title, :brend, :short_name, :image, :url, :bonus, :price, :is_yandex_model, :yandex_id, :category_id)
    end
end
