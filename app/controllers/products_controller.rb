class ProductsController < ApplicationController
  
  
  before_action :set_product, only: [:show, :edit, :update, :reload_form]
#  require "addressable/uri"
  #RestClient.log = $stdout
  

  def index
    session[:without_ym_id] = true if params[:without_ym_id] == 'true'
    session[:without_ym_id] = false if params[:without_ym_id] == 'false'
    
    session[:in_stock] = true if params[:in_stock] == 'true'
    session[:in_stock] = false if params[:in_stock] == 'false'
    
    session[:grid] = params[:grid]
    session[:entity_type] = params[:entity_type_id] unless params[:entity_type_id].blank?
    session[:entity_type] = nil if params[:show_all] == 'true'
    
    @entity_types = EntityType.all#visible_with_products
    
    unless session[:entity_type].blank?
      @entity_type = EntityType.find(session[:entity_type])
      @products = @entity_type.products
    else  
      @products = Product.all
    end
    
    if session[:without_ym_id]
      @products = @products.no_yandex_id.includes(:vendor)
    else  
      @products = @products.includes(:vendor)
    end
    if session[:in_stock]
      @products = @products.in_stock.includes(:vendor)
    else  
      @products = @products.includes(:vendor)
    end
    
    @all_products =
      if session[:in_stock] && session[:without_ym_id]
        Product.no_yandex_id.in_stock.count
      elsif session[:in_stock] && !session[:without_ym_id]
        Product.in_stock.count
      elsif !session[:in_stock] && session[:without_ym_id]
        Product.no_yandex_id.count
      else
        Product.all.count
      end

    if session[:grid].blank?
      @grid = initialize_grid(@products, 
        per_page: 20,
        order: "price",
        order_direction: 'desc'
      )
    else
      grid = 
        if session[:grid].is_a? String
          JSON.parse(session[:grid])
        else
          session[:grid]
        end
      @grid = initialize_grid(@products, 
        per_page: 20,
        order: "#{grid[:order] || 'price'}",
        order_direction: "#{grid[:order_direction]  || 'desc'}",
      )
    end
    @quantity = @products.size
    #@products = @products.paginate(page: params[:page], per_page: 100)
  end



  def scan_models
    session[:grid] = params[:grid]
    
    @entity_types = EntityType.all#visible_with_products
    entity_type_id = params[:entity_type_id]
    @entity_type = EntityType.find(entity_type_id) unless entity_type_id.blank?
    @products_for_scan = Product.no_yandex_id.includes(:vendor)#.limit 300
    @products_for_scan = @products_for_scan.where(entity_type_id: @entity_type.id) if @entity_type
    
    @all_products = @products_for_scan.size
    
    if ScanModel.all.count == 0 or params[:new_scan]
      File.open("log/models.log", 'a') {|file| file.write Time.now; file.write " Начало \n" }
      threads = []
      @pass_scan = []
      @scan_groups = []
      0.upto(1) do |i| 
        @scan_groups << @products_for_scan.limit(@products_for_scan.size / 2).offset((@products_for_scan.size / 2)*i)
      end
      @scan_groups.each do |scans|
        threads << Thread.new do
          scans.each do |s|
            if find_product_yandex(s).any? 
              @pass_scan << s.id 
              ScanModel.create(product_id: s.id)
            end  
          end
        end
      end
      threads.each(&:join)

      File.open("log/models.log", 'a') {|file| file.write Time.now; file.write " Окончание \n \n" }
    end  
    @products = Product.scanned
    @products = @products.where( entity_type_id:  @entity_type.id ) if @entity_type
#    @products = Product.where(id: @products.map(&:id))
    @pass_scan_time = ScanModel.last.created_at + 3.hours
    if session[:grid].blank?
      @grid = initialize_grid(@products, 
        per_page: 20,
        order: "price",
        order_direction: 'desc'
      )
    else
#      render text: session[:grid]
      grid = 
        if session[:grid].class == "String"
          JSON.parse(session[:grid])
        else
          session[:grid]
        end
      @grid = initialize_grid(@products, 
        per_page: 20,
        order: "#{grid[:order] || 'price'}",
        order_direction: "#{grid[:order_direction] || 'desc'}",
      )
    end
    @quantity = @products.size
    
  end
  
  
  def show
    @result = find_product_yandex @product
  end

  def edit
  end


  def update
    respond_to do |format|
      if @product.update(product_params)
        format.html { redirect_to products_url(grid: session[:grid]) }
      else
        format.html { render :show }
      end
    end
  end

  def custom_search
    @searched = get_models_from_yandex URI.encode params[:search]
  end

  def reload_form
    
  end

  private
    
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def product_params
       params.require(:product).permit(:name, :short_name, :yandex_modelid)
    end
end

