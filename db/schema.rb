# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150522075440) do

  create_table "Prod_nasos", force: :cascade do |t|
    t.text "brend",                                limit: 65535
    t.text "tip",                                  limit: 65535
    t.text "patrnumber_artikul_proizvoditelya",    limit: 65535
    t.text "model",                                limit: 65535
    t.text "maksimalnaya_moschnost",               limit: 65535
    t.text "maksimalnaya_proizvoditelnost",        limit: 65535
    t.text "tip_nasosa",                           limit: 65535
    t.text "maksimalnaya_vysota_podachi",          limit: 65535
    t.text "maksimalnaya_vysota_vsasyvaniya",      limit: 65535
    t.text "maksimalnoe_davlenie",                 limit: 65535
    t.text "maksimalnaya_glubina_pogruzheniya",    limit: 65535
    t.text "minimalnyy_ostatochnyy_uroven_vody",   limit: 65535
    t.text "maksimalnaya_temperatura_podachi",     limit: 65535
    t.text "maksimalnyy_razmer_chastits",          limit: 65535
    t.text "ruchka_dlya_perenoski",                limit: 65535
    t.text "vhodnoy_filtr",                        limit: 65535
    t.text "reguliruyuschiy_klapan",               limit: 65535
    t.text "zapornyy_klapan",                      limit: 65535
    t.text "nabor_aksessuarov",                    limit: 65535
    t.text "proizvedeno_v_rf__gtd_nikogda_",       limit: 65535
    t.text "opisanie_dopolnitelnoe_0",             limit: 65535
    t.text "dopolnitelnaya_informatsiya_0",        limit: 65535
    t.text "tip_nasosa_0",                         limit: 65535
    t.text "moschnost_0",                          limit: 65535
    t.text "glubina_pogruzheniya_0",               limit: 65535
    t.text "maksimalnyy_napor_0",                  limit: 65535
    t.text "propusknaya_sposobnost_0",             limit: 65535
    t.text "kachestvo_vody_0",                     limit: 65535
    t.text "razmer_filtruemyh_chastits_0",         limit: 65535
    t.text "diametr_soedineniya_0",                limit: 65535
    t.text "kontrol_urovnya_vody_0",               limit: 65535
    t.text "ves_9043",                             limit: 65535
    t.text "razmery_500598",                       limit: 65535
    t.text "osobennosti_9039",                     limit: 65535
    t.text "maksimalnaya_moschnost_0",             limit: 65535
    t.text "maksimalnaya_proizvoditelnost_0",      limit: 65535
    t.text "maksimalnaya_vysota_podachi_0",        limit: 65535
    t.text "maksimalnoe_davlenie_0",               limit: 65535
    t.text "maksimalnaya_glubina_pogruzheniya_0",  limit: 65535
    t.text "maksimalnaya_temperatura_podachi_0",   limit: 65535
    t.text "maksimalnyy_razmer_chastits_0",        limit: 65535
    t.text "ruchka_dlya_perenoski_0",              limit: 65535
    t.text "vhodnoy_filtr_0",                      limit: 65535
    t.text "reguliruyuschiy_klapan_0",             limit: 65535
    t.text "zapornyy_klapan_0",                    limit: 65535
    t.text "razmery_0",                            limit: 65535
    t.text "minimalnyy_ostatochnyy_uroven_vody_0", limit: 65535
    t.text "osobennosti_0",                        limit: 65535
    t.text "nabor_aksessuarov_0",                  limit: 65535
    t.text "maksimalnaya_vysota_vsasyvaniya_0",    limit: 65535
    t.text "srok_garantii_0",                      limit: 65535
  end

  create_table "a1s", force: :cascade do |t|
    t.string   "art_no",          limit: 16
    t.string   "category",        limit: 128
    t.string   "description",     limit: 1000
    t.decimal  "price1",                       precision: 18, scale: 2
    t.decimal  "price2",                       precision: 18, scale: 2
    t.integer  "quantity",        limit: 4
    t.integer  "order_quantity",  limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "is_on_demand",    limit: 1
    t.decimal  "price3",                       precision: 18, scale: 2
    t.decimal  "price4",                       precision: 18, scale: 2
    t.datetime "last_date_in"
    t.datetime "next_date_in"
    t.datetime "last_date_exist"
    t.string   "barcode1",        limit: 250
    t.string   "barcode2",        limit: 250
    t.string   "barcode3",        limit: 250
    t.string   "warranty",        limit: 250
    t.boolean  "scheduled",       limit: 1
    t.datetime "planned_date"
    t.boolean  "executed",        limit: 1
    t.string   "supplier_no",     limit: 255
    t.string   "supplier_name",   limit: 100
    t.integer  "product_id",      limit: 4
    t.boolean  "supplier_offer",  limit: 1,                             default: false, null: false
  end

  add_index "a1s", ["product_id"], name: "Index_2", using: :btree

  create_table "ar_rates", force: :cascade do |t|
    t.integer  "resource_id",   limit: 4,                              null: false
    t.string   "resource_type", limit: 255,                            null: false
    t.integer  "author_id",     limit: 4,                              null: false
    t.string   "author_type",   limit: 255,                            null: false
    t.decimal  "value",                     precision: 10, default: 0
    t.datetime "created_at",                                           null: false
    t.datetime "updated_at",                                           null: false
  end

  add_index "ar_rates", ["author_id", "author_type"], name: "index_ar_rates_on_author_id_and_author_type", using: :btree
  add_index "ar_rates", ["resource_id", "resource_type"], name: "index_ar_rates_on_resource_id_and_resource_type", using: :btree

  create_table "ar_ratings", force: :cascade do |t|
    t.integer  "resource_id",   limit: 4,                              null: false
    t.string   "resource_type", limit: 255,                            null: false
    t.string   "type",          limit: 255
    t.integer  "total",         limit: 4,                  default: 0
    t.integer  "sum",           limit: 4,                  default: 0
    t.decimal  "average",                   precision: 10, default: 0
    t.decimal  "estimate",                  precision: 10, default: 0
    t.datetime "created_at",                                           null: false
    t.datetime "updated_at",                                           null: false
  end

  add_index "ar_ratings", ["resource_id", "resource_type"], name: "index_ar_ratings_on_resource_id_and_resource_type", using: :btree

  create_table "articles", force: :cascade do |t|
    t.integer "entity_type_id", limit: 4
    t.text    "text",           limit: 65535
    t.text    "intro",          limit: 65535, null: false
  end

  create_table "buffer_prices", force: :cascade do |t|
    t.integer  "product_id", limit: 4
    t.float    "price",      limit: 24,  null: false
    t.string   "source",     limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "buffer_prices", ["created_at"], name: "index_buffer_prices_on_created_at", using: :btree
  add_index "buffer_prices", ["product_id"], name: "index_buffer_prices_on_product_id", using: :btree

  create_table "calendar", force: :cascade do |t|
    t.date    "date"
    t.integer "calendar_type_id", limit: 4
    t.string  "description",      limit: 255
  end

  add_index "calendar", ["date"], name: "index_calendar_on_date", using: :btree

  create_table "calendar_types", force: :cascade do |t|
    t.string "code", limit: 25, null: false
    t.string "name", limit: 50, null: false
  end

  create_table "cart_items", force: :cascade do |t|
    t.integer "product_id", limit: 4,   null: false
    t.integer "quantity",   limit: 4,   null: false
    t.string  "user_guid",  limit: 256, null: false
    t.date    "created_at"
    t.date    "updated_at"
    t.integer "price",      limit: 4,   null: false
  end

  add_index "cart_items", ["product_id"], name: "index_cart_items_on_product_id", using: :btree
  add_index "cart_items", ["user_guid"], name: "index_cart_items_on_user_guid", length: {"user_guid"=>255}, using: :btree

  create_table "catalog", primary_key: "Id", force: :cascade do |t|
    t.integer "ParentId",         limit: 4
    t.string  "Name",             limit: 64,  null: false
    t.string  "ShortDescription", limit: 512, null: false
    t.string  "Path",             limit: 512, null: false
  end

  add_index "catalog", ["ParentId"], name: "FK_Catalog_Catalog", using: :btree

  create_table "catalog_trees", force: :cascade do |t|
    t.string   "name",               limit: 50,                  null: false
    t.string   "singular_name",      limit: 50,                  null: false
    t.string   "description",        limit: 500
    t.integer  "parent_id",          limit: 4
    t.boolean  "is_disabled",        limit: 1,   default: false, null: false
    t.integer  "position",           limit: 4
    t.string   "image",              limit: 200
    t.string   "image_file_name",    limit: 255
    t.string   "image_content_type", limit: 255
    t.integer  "image_file_size",    limit: 4
    t.datetime "image_updated_at"
    t.string   "slug",               limit: 255
  end

  add_index "catalog_trees", ["is_disabled"], name: "Index_3", using: :btree
  add_index "catalog_trees", ["name"], name: "Index_5", using: :btree
  add_index "catalog_trees", ["parent_id", "position", "name"], name: "Index_7", using: :btree
  add_index "catalog_trees", ["parent_id"], name: "Index_8", using: :btree
  add_index "catalog_trees", ["position", "name"], name: "Index_4", using: :btree
  add_index "catalog_trees", ["position"], name: "Index_6", using: :btree
  add_index "catalog_trees", ["slug"], name: "index_catalog_trees_on_slug", using: :btree

  create_table "catalog_trees_entity_types", id: false, force: :cascade do |t|
    t.integer "catalog_tree_id", limit: 4
    t.integer "entity_type_id",  limit: 4
  end

  add_index "catalog_trees_entity_types", ["catalog_tree_id"], name: "FK_catalog_trees_entity_types_1", using: :btree
  add_index "catalog_trees_entity_types", ["entity_type_id"], name: "FK_catalog_trees_entity_types_2", using: :btree

  create_table "comments", force: :cascade do |t|
    t.string   "title",            limit: 50
    t.text     "comment",          limit: 65535
    t.integer  "commentable_id",   limit: 4
    t.string   "commentable_type", limit: 255
    t.integer  "user_id",          limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "comments", ["commentable_id"], name: "index_comments_on_commentable_id", using: :btree
  add_index "comments", ["commentable_type"], name: "index_comments_on_commentable_type", using: :btree
  add_index "comments", ["user_id"], name: "index_comments_on_user_id", using: :btree

  create_table "currency_rates", force: :cascade do |t|
    t.decimal  "value",                    precision: 10, scale: 2
    t.datetime "updated_at",                                        null: false
    t.float    "cashless_rate", limit: 24
  end

  create_table "data_types", force: :cascade do |t|
    t.string "name",  limit: 50
    t.string "alias", limit: 20
    t.string "sql",   limit: 20
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   limit: 4,     default: 0
    t.integer  "attempts",   limit: 4,     default: 0
    t.text     "handler",    limit: 65535
    t.text     "last_error", limit: 65535
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by",  limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "delivery_options", force: :cascade do |t|
    t.string  "name",               limit: 256
    t.integer "price",              limit: 4,   null: false
    t.integer "min_order_total",    limit: 4,   null: false
    t.integer "max_order_total",    limit: 4,   null: false
    t.boolean "is_actual_delivery", limit: 1,   null: false
    t.integer "position",           limit: 4,   null: false
    t.string  "aliace",             limit: 50
  end

  create_table "demand_deliveries", force: :cascade do |t|
    t.string  "name",     limit: 50,              null: false
    t.string  "display",  limit: 250,             null: false
    t.integer "min_days", limit: 4,   default: 0, null: false
    t.integer "max_days", limit: 4,   default: 0, null: false
  end

  create_table "descriptions", force: :cascade do |t|
    t.integer  "product_description_id", limit: 4
    t.string   "source",                 limit: 255
    t.text     "description",            limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "raw_data",               limit: 65535
  end

  add_index "descriptions", ["product_description_id", "source"], name: "unique_description_for_source", unique: true, using: :btree

  create_table "entity_types", force: :cascade do |t|
    t.string   "table_name",               limit: 50
    t.string   "display_name",             limit: 50
    t.string   "display_name_singular",    limit: 50
    t.string   "description",              limit: 2000
    t.string   "alias",                    limit: 50
    t.boolean  "is_product",               limit: 1
    t.string   "name",                     limit: 45
    t.boolean  "is_searchable",            limit: 1
    t.boolean  "is_disabled",              limit: 1
    t.string   "category_1c",              limit: 256,                  null: false
    t.boolean  "export_to_ym",             limit: 1,    default: false
    t.boolean  "build_short_desc",         limit: 1
    t.string   "seo_title",                limit: 2000,                 null: false
    t.string   "seo_description",          limit: 2000,                 null: false
    t.string   "seo_key_words",            limit: 2000,                 null: false
    t.integer  "delivery_cost",            limit: 4
    t.integer  "free_delivery_min_price",  limit: 4
    t.string   "ym_type_prefix_type_id",   limit: 250
    t.integer  "grabber_priority",         limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "parent_id",                limit: 4
    t.string   "ruby_class",               limit: 255
    t.float    "supplier_value_added",     limit: 24
    t.float    "supplier_value_added_min", limit: 24
    t.string   "slug",                     limit: 255
  end

  add_index "entity_types", ["parent_id"], name: "index_entity_types_on_parent_id", using: :btree
  add_index "entity_types", ["slug"], name: "index_entity_types_on_slug", using: :btree

  create_table "products", force: :cascade do |t|
    t.integer  "price",              limit: 4
    t.string   "invent_no",          limit: 16
    t.integer  "entity_type_id",     limit: 4
    t.integer  "product_desc_id",    limit: 4
    t.integer  "stock_quantity",     limit: 4
    t.text     "description",        limit: 4294967295
    t.text     "image_file_name",    limit: 65535
    t.string   "guarranty",          limit: 100
    t.string   "type",               limit: 45
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "is_on_demand",       limit: 1
    t.boolean  "dont_export_to_ym",  limit: 1
    t.decimal  "price_in",                              precision: 18, scale: 2
    t.integer  "cashless_price",     limit: 4
    t.decimal  "rating_average",                        precision: 6,  scale: 2
    t.datetime "last_date_in"
    t.string   "vendor_code",        limit: 50
    t.string   "bar_code",           limit: 30
    t.string   "ym_model_id",        limit: 50
    t.integer  "vendor_id",          limit: 4
    t.string   "model_name",         limit: 255
    t.decimal  "height2",                               precision: 8,  scale: 2
    t.decimal  "width2",                                precision: 8,  scale: 2
    t.decimal  "depth2",                                precision: 8,  scale: 2
    t.decimal  "weight2",                               precision: 8,  scale: 2
    t.string   "cached_slug",        limit: 255
    t.string   "image_content_type", limit: 30
    t.integer  "image_file_size",    limit: 4
    t.datetime "image_updated_at"
    t.datetime "next_date_in"
    t.datetime "last_date_exist"
    t.boolean  "no_update",          limit: 1,                                   default: false, null: false
    t.boolean  "scheduled",          limit: 1
    t.datetime "planned_date"
    t.boolean  "executed",           limit: 1
    t.string   "supplier_no",        limit: 50
    t.string   "supplier_name",      limit: 100
    t.integer  "yandex_modelid",     limit: 4
    t.integer  "yandex_hid",         limit: 4
    t.string   "price_source",       limit: 30
    t.string   "short_name",         limit: 500
    t.datetime "props_updated_at"
    t.string   "fake_file_name",     limit: 255
    t.string   "fake_content_type",  limit: 255
    t.integer  "fake_file_size",     limit: 4
    t.datetime "fake_updated_at"
    t.datetime "grabbed_at"
    t.string   "slug",               limit: 255
    t.string   "name",               limit: 500
    t.datetime "b2b_updated_at"
    t.integer  "delivery_order",     limit: 4,                                   default: 7,     null: false
    t.boolean  "vendor_warranty",    limit: 1,                                   default: true,  null: false
    t.decimal  "sale",                                  precision: 18, scale: 3, default: 1.0,   null: false
    t.integer  "sales_tag_id",       limit: 4
    t.text     "name_market",        limit: 65535
    t.integer  "supplier_id",        limit: 4
    t.string   "demand_delivery",    limit: 25
    t.integer  "delivery_term_id",   limit: 4
  end

  add_index "products", ["cached_slug"], name: "index_products_on_cached_slug", unique: true, using: :btree
  add_index "products", ["created_at"], name: "index_products_on_created_at", using: :btree
  add_index "products", ["delivery_order"], name: "index_products_on_delivery_order", using: :btree
  add_index "products", ["delivery_term_id"], name: "index_products_on_delivery_term_id", using: :btree
  add_index "products", ["entity_type_id", "price", "stock_quantity"], name: "main_selec", using: :btree
  add_index "products", ["entity_type_id"], name: "FK_Product_Product", using: :btree
  add_index "products", ["invent_no"], name: "invent_no", unique: true, using: :btree
  add_index "products", ["model_name"], name: "index_products_on_model_name", using: :btree
  add_index "products", ["name"], name: "index_products_on_name", length: {"name"=>255}, using: :btree
  add_index "products", ["no_update"], name: "index_products_on_no_update", using: :btree
  add_index "products", ["price"], name: "index_products_on_price", using: :btree
  add_index "products", ["sales_tag_id"], name: "index_products_on_sales_tag_id", using: :btree
  add_index "products", ["slug"], name: "index_products_on_slug", using: :btree
  add_index "products", ["stock_quantity"], name: "index_products_on_stock_quantity", using: :btree
  add_index "products", ["supplier_id"], name: "index_products_on_supplier_id", using: :btree
  add_index "products", ["supplier_name"], name: "index_products_on_supplier_name", using: :btree
  add_index "products", ["supplier_no"], name: "index_products_on_supplier_no", using: :btree
  add_index "products", ["vendor_id"], name: "vendor_id", using: :btree

  create_table "scan_models", force: :cascade do |t|
    t.integer  "product_id", limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "sub_vendors", force: :cascade do |t|
    t.string  "name",        limit: 256
    t.string  "website",     limit: 256
    t.boolean "is_disabled", limit: 1,   default: false, null: false
    t.string  "slug",        limit: 255
  end

  add_index "sub_vendors", ["name"], name: "vendors_name", length: {"name"=>255}, using: :btree
  add_index "sub_vendors", ["slug"], name: "index_sub_vendors_on_slug", using: :btree

  create_table "visited_products", force: :cascade do |t|
    t.string   "user_guid",  limit: 1000
    t.integer  "product_id", limit: 4
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "visited_products", ["product_id"], name: "index_visited_products_on_product_id", using: :btree
  add_index "visited_products", ["user_guid"], name: "index_visited_products_on_user_guid", length: {"user_guid"=>255}, using: :btree

  create_table "yandex_word_statistics", force: :cascade do |t|
    t.string   "product_name",           limit: 45
    t.string   "trancated_product_name", limit: 45
    t.integer  "results_qty",            limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "session_id",             limit: 4
  end

  create_table "yandex_wordstats", force: :cascade do |t|
    t.string   "product_name",           limit: 255,   null: false
    t.text     "product_name_truncated", limit: 65535
    t.integer  "product_quantity",       limit: 4
    t.integer  "keyword_quantity",       limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "session_id",             limit: 4,     null: false
  end

  add_index "yandex_wordstats", ["product_name"], name: "index_yandex_wordstats_on_product_name", unique: true, using: :btree
  add_index "yandex_wordstats", ["session_id"], name: "index_yandex_wordstats_on_session_id", using: :btree

  create_table "ym_categories", force: :cascade do |t|
    t.string  "name",  limit: 250
    t.string  "url",   limit: 2000
    t.boolean "nouse", limit: 1,    default: false
  end

  create_table "ym_offers", force: :cascade do |t|
    t.integer  "product_price",  limit: 4,                null: false
    t.integer  "delivery_price", limit: 4,                null: false
    t.string   "company_name",   limit: 250,              null: false
    t.integer  "position",       limit: 4,                null: false
    t.string   "product_url",    limit: 2000,             null: false
    t.string   "product_name",   limit: 1000,             null: false
    t.integer  "ym_product_id",  limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "company_rating", limit: 4,    default: 0
  end

  add_index "ym_offers", ["ym_product_id"], name: "ym_product_id", using: :btree

  create_table "ym_products", force: :cascade do |t|
    t.string   "name",                        limit: 250
    t.string   "url",                         limit: 2000
    t.integer  "category_id",                 limit: 4
    t.integer  "our_product_id",              limit: 4
    t.integer  "our_offer_id",                limit: 4
    t.integer  "best_offer_id",               limit: 4
    t.integer  "best_offer_with_delivery_id", limit: 4
    t.integer  "position",                    limit: 4
    t.integer  "session_id",                  limit: 4
    t.integer  "yandex_queries_qty",          limit: 4
    t.string   "yandex_query_keyword",        limit: 250
    t.datetime "yandex_query_created_at"
    t.integer  "offers_qty",                  limit: 4
    t.integer  "second_best_offer_id",        limit: 4
    t.integer  "ws_spb_count",                limit: 4
    t.datetime "ws_date"
    t.integer  "modelid",                     limit: 4
    t.integer  "hid",                         limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ym_products", ["name"], name: "YmProducts_Name", using: :btree
  add_index "ym_products", ["session_id"], name: "YmProducts_Session", using: :btree

  create_table "ym_sessions", force: :cascade do |t|
    t.datetime "created_at",                            null: false
    t.boolean  "is_complete", limit: 1, default: false, null: false
  end

  add_foreign_key "catalog", "catalog", column: "ParentId", primary_key: "Id", name: "FK_Catalog_Catalog"
  add_foreign_key "catalog_trees", "catalog_trees", column: "parent_id", name: "FK_catalog_trees_parent_id"
end
